//Actions to be performed when the Meteor application is started
Meteor.startup(function() {
    /**********
     TICKETS
     **********/

    var ticketId = 1;

    // Create fake tickets if none exist
    Factory.define('ticket', Tickets, {
        id: function() {
            return ticketId++;
        },
        summary: function() {
            return Fake.fromArray([
                'Display driver stopped responding',
                'Computer freezes a couple times on start-up',
                'Internet connection keeps cutting out',
                'Cannot find a file I saved',
                'How can I encrypt a file?',
                'Cannot open a file. How do I fix the formatting?',
                'Computer will not boot',
                'Cannot access site on mobile device'
            ]);
        },
        creator: function() {
            return Fake.fromArray(['Customer']);
        },
        assignedTo: function() {
            return Fake.user({
                fields: ['fullname']
            });
        },
        created: function() {
            return moment().subtract(_.random(4,10), 'days').format('MM/DD/YYYY');
        },
        due: function() {
            return moment().subtract(_.random(1,4), 'days').format('MM/DD/YYYY');
        },
        priority: function() {
            return Fake.fromArray(['Low', 'Med', 'High']);
        },
        status: function() {
            return Fake.fromArray(['Open', 'Closed']);
        }
    });

    // Generate new tickets each time server is refreshed
    /*
     if (Tickets.find({}).count() > 0) {
     Tickets.remove({});
     }
     */

    // Generate tickets if none exist
    if (Tickets.find({}).count() === 0) {

        _(50).times(function(n) {
            Factory.create('ticket');
        });
    }

    /**********
     USERS
     **********/

    var users = [{
        name: "Admin",
        email: "admin@triage.com",
        roles: ['admin']
    }, {
        name: "Agent",
        email: "agent@triage.com",
        roles: ['agent']
    }, {
        name: "Customer",
        email: "customer@triage.com",
        roles: ['customer']
    }];

    // Create users if none exist
    if (Meteor.users.find().count() < 1) {
        _.each(users, function(user) {
            var id;
            id = Accounts.createUser({
                email: user.email,
                password: "password",
                profile: {
                    name: user.name
                }
            });

            // Set email verified to yes
            Meteor.users.update(id, {$set: {"emails.0.verified": true}});

            // Add user to specified role
            Roles.addUsersToRoles(id, user.roles);
        });
    }
});
