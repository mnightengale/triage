//These methods are called by the client and validation is performed on the server.
//The result is then passed back to the client. This is done for security and
//performance reasons.

//These methods are used on the Manage page
Meteor.methods({
    validateUser: function(user) {
        // Check to see if user is admin
        if (Roles.userIsInRole(user, 'admin')) {
            return true;
        } else {
            throw Meteor.Error(403, "Not authorized to add new users");
        }
    },
    addUser: function(username, email, password, role) {
        var id = Accounts.createUser(username, email, password);
        Meteor.users.update(id, {$set: {"emails.0.verified": true}});
    },
    addUserRole: function(username, role) {
        var id = Meteor.users.findOne({profile: {name: username}})._id;
        Roles.addUsersToRoles(id, role);
    },
    deleteUser: function(emailAddress) {
        // Remove user
        Meteor.users.remove({emails: {$elemMatch: {address: emailAddress}}});
    },
    addTicket: function(id) {
        console.log(id);
        Tickets.insert(id);
    },
    resetTickets: function() {
        // Delete existing tickets
        Tickets.remove({});

        // Generate new tickets
        _(30).times(function(n) {
            Factory.create('ticket');
        });
    },
        updateValue: function (fieldID, ticketID, updatedValue) {
            //Convert ticket ID to string
            var stringTicketID = parseInt(ticketID);

            //Grab unique ticket ID
            var uniqueID = Tickets.findOne({id: stringTicketID})._id;

            switch (fieldID) {
                case "Creator":
                    Tickets.update({_id: uniqueID}, {$set: {creator: updatedValue}});
                    break;
                case "Assignee":
                    Tickets.update({_id: uniqueID}, {$set: {assignedTo: {fullname: updatedValue}}});
                    break;
                case "Due":
                    Tickets.update({_id: uniqueID}, {$set: {due: updatedValue}});
                    break;
                case "Priority":
                    Tickets.update({_id: uniqueID}, {$set: {priority: updatedValue}});
                    break;
                case "Status":
                    Tickets.update({_id: uniqueID}, {$set: {status: updatedValue}});
                    break;
                case "Summary":
                    Tickets.update({_id: uniqueID}, {$set: {summary: updatedValue}});
                    break;
            }
        }
    });
