//Determines what actions are allowed on the Tickets collection
Tickets.allow({
    'insert': function(userId, doc) {
        return userId;
    },
    'update': function(userId, doc, fields, modifier) {
        return userId;
    },
    'remove': function(userId, doc) {
        return userId;
    }
});
