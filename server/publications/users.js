//Returns email, profile name, and role of all users
//https://github.com/englue/meteor-publish-composite
Meteor.publishComposite("users", function() {
    return {
        find: function() {
            return Meteor.users.find({}, {fields: {emails: 1, profile: 1, roles: 1}});
        }
        // ,
        // children: [
        //   {
        //     find: function(item) {
        //       return [];
        //     }
        //   }
        // ]
    }
});
