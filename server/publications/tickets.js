//Returns all tickets to connected clients
//https://github.com/englue/meteor-publish-composite
Meteor.publishComposite("tickets", function() {
    return {
        find: function() {
            //Return tickets by newest first
            return Tickets.find({}, {
                sort: {id: -1}
            });
        }
        // ,
        // children: [
        //   {
        //     find: function(item) {
        //       return [];
        //     }
        //   }
        // ]
    }
});
