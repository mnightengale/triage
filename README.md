# README #

### How do I get set up? ###
* Download repository or clone (best place is probably your desktop)
* Open a command prompt and navigate to project directory
* Type "meteor" and the project will build and run
* Open a web browser and navigate to http://localhost:3000

### Emails & Passwords ###
**Emails**:

* admin@triage.com
* agent@triage.com
* customer@triage.com

**Password**:

* password

### Contribution guidelines ###
*TODO: Fill this out later*

* Writing tests
* Code review
* Other guidelines