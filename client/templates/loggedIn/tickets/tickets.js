//Notification
toastr.options = {
    "positionClass": "toast-bottom-right",
};

Template.tickets.rendered = function() {
    // Hide closed tickets
    $("#closedTickets").hide();

    // Add red background for High priority tickets
    $("td:contains('High')").closest("tr").addClass("danger");

    // Initialize sidebar with parameters
    $(".sidebar.bottom").sidebar({
        side: "bottom"
    });

    sidebarOpen = false;
};

Template.tickets.events({
    //Toggle between open and closed tickets depending on what is active
    'click .btnViewTickets': function (event) {
        if ($("#closedTickets").is(":hidden")) {
            $("#openTickets").slideUp("slow", function () {
                $("#closedTickets").slideDown("slow");
                $(".btnViewTickets").text("View Open Tickets");
            });
        }

        if ($("#openTickets").is(":hidden")) {
            $("#closedTickets").slideUp("slow", function () {
                $("#openTickets").slideDown("slow");
                $(".btnViewTickets").text("View Closed Tickets");
            });
        }
    },
    //Close sidebar when "Close" button is clicked
    'click .sidebarCloseBtn': function (event) {
        $(".sidebar.bottom").trigger("sidebar:close");
        $(".sidebar.bottom").on("sidebar:closed", function () {
            sidebarOpen = false;
        });
    },
    //Populate sidebar with ticket information when ID is clicked
    'click .idClick': function (event) {
        //If sidebar is not already open, open it. Otherwise, keep it open until user closes it.
        if (sidebarOpen == false) {
            $(".sidebar.bottom").trigger("sidebar:open"); //Open sidebar
            $(".sidebar.bottom").on("sidebar:opened", function () {
                sidebarOpen = true;
            });
        }

        //Grab ticket fields and populate. Also trims extra whitespace.
        $(".sidebarDetailsTable > tr > td#ID").text(_.clean($(event.target).text()));
        $(".sidebarDetailsTable > tr > td >  input#Creator").val(_.clean($(event.target).closest("tr").children("td.creatorField").text()));
        $(".sidebarDetailsTable > tr > td > input#Assignee").val(_.clean($(event.target).closest("tr").children("td.assigneeField").text()));
        $(".sidebarDetailsTable > tr > td#Created").text(_.clean($(event.target).closest("tr").children("td.createdField").text()));
        $(".sidebarDetailsTable > tr > td > input#Due").val(_.clean($(event.target).closest("tr").children("td.dueField").text()));
        $(".sidebarDetailsTable > tr > td > select#Priority").val(_.clean($(event.target).closest("tr").children("td.priorityField").text()));
        $(".sidebarDetailsTable > tr > td > select#Status").val(_.clean($(event.target).closest("tr").children("td.statusField").text()));

        $(".sidebarSummaryTable > tr > input#Summary").val(_.clean($(event.target).closest("tr").children("td.summaryField").text()));
    },
    //Update value in database when user updates value
    'blur .sidebarContent input': function (event) {
        //Grab ticket ID
        var ticketID = $(".sidebarDetailsTable > tr > td#ID").text();

        //Grab unique selector for input field
        var fieldID = $(event).get(0).target.id;

        //Grab updated value from input field
        var updatedValue = $(event.target).val();

        //Update database depending on what field was just changed
        Meteor.call("updateValue", fieldID, ticketID, updatedValue);
    },
    'change select': function(event) {
        //Grab ticket ID
        var ticketID = $(".sidebarDetailsTable > tr > td#ID").text();

        //Grab unique selector for input field
        var fieldID = $(event).get(0).target.id;

        //Grab updated value from input field
        var updatedValue = $(event.target).val();

        //Update database depending on what field was just changed
        Meteor.call("updateValue", fieldID, ticketID, updatedValue);
    },

    //Add New Ticket into database
    "click #createTicketSubmit": function (event) {
		//Get last ticket ID
		var lastTicketID = Tickets.find().count() + 1;
		
        var summary = document.getElementById("inputSummary").value;
        var creator = document.getElementById("inputCreator").value;
        var assign = document.getElementById("inputAssign").value;
        var create = document.getElementById("inputCreate").value;
        var due = document.getElementById("inputDue").value;
        var priority = document.getElementById("inputPriority").value;
        var status = document.getElementById("inputStatus").value;
        var id = lastTicketID;

        //Store values
        var ticketValues = {
            id: id,
            summary: summary,
            creator: creator,
            assignedTo: {
                fullname: assign
            },
            created: create,
            due: due,
            priority: priority,
            status: status
        }

        Meteor.call("addTicket", ticketValues, function (error, result) {
            if (error) {
                toastr.error("Error occurred while creating ticket");
            } else {
                toastr.success("Ticket created");
            }
            $('#createTicketModal').modal('hide');
        });
    }
});