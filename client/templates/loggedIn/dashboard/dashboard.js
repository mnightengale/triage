//Helpers for chart counters
Template.dashboard.helpers({
    closedTickets: function() {
        return Tickets.find({
            status: "Closed"
        }).count();
    },
    openTickets: function() {
        return Tickets.find({
            status: "Open"
        }).count();
    },
    unassignedTickets: function() {
        return Tickets.find({
            assignedTo: null
        }).count();
    },
    overdueTickets: function() {
        return Tickets.find({
            due: {
                $lt: new Date()
            }
        }).count();
    }
});

//This code runs each time the dashboard page is rendered
Template.dashboard.rendered = function() {
    // Set chart options and initial data values
    var data = [
        {
            value: 0,
            color:"#A94442",
            highlight: "#c6615f",
            label: "Overdue"
        },
        {
            value: 0,
            color: "#3C763D",
            highlight: "#579358",
            label: "Open"
        },
        {
            value: 0,
            color: "#8A6D3B",
            highlight: "#a78954",
            label: "Unassigned"
        },
        {
            value: 0,
            color: "#0B75B0",
            highlight: "#3895c8",
            label: "Closed"
        }
    ]

    //Set legend template
    var options = [
        {
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        }
    ]

    //Update ticket chart when reactive data updates
    Tracker.autorun(function(){
        var overdueTickets = Tickets.find({due: {$lt: new Date()}}).count();
        var openTickets = Tickets.find({status: "Open"}).count();
        var unassignedTickets = Tickets.find({assignedTo: null}).count();
        var closedTickets = Tickets.find({status: "Closed"}).count();

        var ctx = $("#statusChart").get(0).getContext("2d");
        var statusChart = new Chart(ctx).Doughnut(data,options);

        statusChart.generateLegend(); //Returns HTML string of a legend for this chart

        //Assign dynamic values to the chart
        statusChart.segments[0].value = overdueTickets;
        statusChart.segments[1].value = openTickets;
        statusChart.segments[2].value = unassignedTickets;
        statusChart.segments[3].value = closedTickets;
        statusChart.update();
    });
}
