//Notification
toastr.options = {
    "positionClass": "toast-bottom-right",
}

Template.manage.events({
    //Grab values when user clicks on submit button in Create User dialog box
    "click #createUserSubmit": function(event) {
        var username = document.getElementById("inputUsername").value;
        var email = document.getElementById("inputEmail").value;
        var password = document.getElementById("inputPassword").value;
        var role = document.getElementById("inputRole").value;

        //Store values
        var userValues = {
            profile: {
                name: username
            },
            email: email,
            password: password
        };

        console.log(userValues);

        // Validate that logged in user is allowed to add new user
        Meteor.call("validateUser", Meteor.user(), function(error, result) {
            if (error) {
                toastr.error("Not authorized to add new users");
            }
            if (result) {
                Meteor.call("addUser", userValues, function(error) {
                    if (error) {
                        toastr.error("Error occurred while creating user");
                    } else {
                        //Add user to role
                        Meteor.call("addUserRole", username, role, function(error) {
                            if (error) {
                                toastr.error("Error occurred while adding user to role");
                            } else {
                                toastr.success("User created");
                            }
                        });
                    }
                    $('#createUserModal').modal('hide');
                });
            }
        });
    },
    //Validate that logged in user is allowed to delete existing user
    "click .btnDeleteUser": function(event) {
        Meteor.call("validateUser", Meteor.user(), function(error, result) {
            if (error) {
                toastr.error("Not Authorized");
            }
            //If authorized
            if (result) {
                //Grab email
                var email = _.trim($(event.target).closest('tr').children('td#email').text());

                Meteor.call("deleteUser", email, function(error, result) {
                    if (error) {
                        toastr.error("Error occurred while deleting user");
                    } else {
                        toastr.success("User deleted");
                    }
                });
            }
        });
    },
    //Validate that logged in user can reset tickets
    "click .btnResetTickets": function(event) {
        Meteor.call("validateUser", Meteor.user(), function(error, result) {
            if (error) {
                toastr.error("Not Authorized");
            }
            if (result) {
                Meteor.call("resetTickets", function(error, result) {
                    if (error) {
                        toastr.error("Error occurred while resetting tickets");
                    } else {
                        toastr.info("Existing tickets deleted &<br> new tickets generated", "Tickets Reset");
                    }
                });
            }
        });
    }
});
