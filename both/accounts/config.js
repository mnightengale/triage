//Sets various options for Iron Router and the accounts package

//Return user to sign in page when they logout
var logoutFunc = function() {
  Router.go('/sign-in');
};

//Tells Iron Router to apply appLayout layout when certain pages are requested
AccountsTemplates.configureRoute('signIn', {layoutTemplate: 'appLayout', redirect: '/dashboard'});
AccountsTemplates.configureRoute('ensureSignedIn', {layoutTemplate: 'appLayout', redirect: '/dashboard'});

//Configuration options for User Accounts package - https://github.com/meteor-useraccounts/core/blob/master/Guide.md
AccountsTemplates.configure({
    // Behaviour
    confirmPassword: true,
    enablePasswordChange: false,
    forbidClientAccountCreation: true,
    overrideLoginErrors: true,
    sendVerificationEmail: false,
    lowercaseUsername: false,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showPlaceholders: false,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Redirects
    homeRoutePath: '/dashboard',
    redirectTimeout: 4000,

    // Hooks
    onLogoutHook: logoutFunc,
    //onSubmitHook: submitFunc,
    //preSignUpHook: myPreSubmitFunc,

    // Texts
    texts: {
      title: {
        signIn: "Triage Help Desk"
      }
    }
});
