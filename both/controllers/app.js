//Set up controllers for Iron Router and logout
AppController = RouteController.extend({
    layoutTemplate: 'appLayout'
});

AppController.events({
    'click [data-action=logout]' : function() {
        AccountsTemplates.logout();
    }
});
