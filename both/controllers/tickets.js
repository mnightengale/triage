TicketsController = AppController.extend({
    waitOn: function() {
        return this.subscribe('tickets'); //Returns tickets after page has completely loaded
    },
    data: {
        //Return tickets with certain status
        openTickets: Tickets.find({status: "Open"}),
        closedTickets: Tickets.find({status: "Closed"})
    },
    onAfterAction: function () {
        Meta.setTitle('Tickets'); //Sets title after tickets have been loaded
    }
});

//TODO: This is just a template in case something else needs to be added. Remove before deploy.
/*
 TicketsController.events({
 'click [data-action=doSomething]': function (event, template) {
 event.preventDefault();
 }
 });
 */
