DashboardController = AppController.extend({
    waitOn: function() {
        return this.subscribe('tickets'); //Returns tickets after page has completely loaded
    },
    data: {
        items: Tickets.find({}) //Returns all tickets in "Tickets" collection
    },
    onBeforeAction: function() {
        this.next();
    },
    onAfterAction: function () {
        Meta.setTitle('Dashboard'); //Sets title after tickets have been loaded
    }
});
