ManageController = AppController.extend({
  waitOn: function() {
    return this.subscribe('users'); //Returns users from Users collection
  },
  data: {
    users: Meteor.users.find({}) //Return all users
  },
  onAfterAction: function () {
    Meta.setTitle('Manage'); //Set title after data is returned
  }
});

ManageController.events({
  'click [data-action=deleteUser]': function (event) {
    event.preventDefault(); //Prevent page from reloading when user clicks on delete user button
  }
});
