//Set title and suffix on client connected to application
if(Meteor.isClient) {
  Meta.config({
      options: {
        // Meteor.settings[Meteor.settings.environment].public.meta.title
        title: 'Triage',
        suffix: 'Triage'
      }
  });
}
