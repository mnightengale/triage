//Defines routes for Iron Router - https://iron-meteor.github.io/iron-router/#route-parameters
Router.route('/', function() {
    Router.go('/dashboard');
});

Router.route('/dashboard', {
    name: 'dashboard',
    controller: 'DashboardController'
});

Router.route('/tickets', {
    name: 'tickets',
    controller: 'TicketsController'
});

Router.route('/manage', {
    name: 'manage',
    controller: 'ManageController'
});

Router.plugin('ensureSignedIn', {
    except: ['atSignIn']
});
