//Defines controller for Iron Router and template to use while page/data is loading
Router.configure({
    controller: 'AppController',
    loadingTemplate: 'loading',
    layoutTemplate: 'appLayout'
});

// Router.plugin('loading', {loadingTemplate: 'loading'});
Router.plugin('dataNotFound', {dataNotFoundTemplate: 'notFound'});
